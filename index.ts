import express, { Request, Response, NextFunction } from 'express';
import { json } from 'body-parser'
import User from './user_model'
import { BadRequestError, ConflictError, IError, NotFoundError } from "./error_model"

const app = express();

// Middleware example
app.use(json())

const PORT = process.env.PORT || 6000

const users: User[] = [new User("test@test.com", "9085198eoeuiuffuo1024845", 22)]

app.get('/users', (req: Request, res: Response) => {
    const santizedUsers = users.map(user => user.getUser())
    return res.status(200).json({ users: santizedUsers })
})

app.post('/users', (req: Request, res: Response, next) => {
    const { email, password, age } = req.body

    if (!email) {
        throw new BadRequestError('Email is required')
    }
    if (!password) {
        throw new BadRequestError('Password is required')
    }

    const existingUser = users.find(user => user.email === email)
    if (existingUser) {
        throw new ConflictError('User already exists')
    }

    const user: User = new User(email, password)
    if (age) {
        user.age = age
    }

    users.push(user)
    return res.status(201).json({ message: "User successfully created.", user: user.getUser() })

})

app.delete('/users/:id', (req: Request, res: Response) => {
    const { id } = req.params

    const userToDeleteIndex = users.findIndex(user => user.id === id)

    if (userToDeleteIndex === -1) {
        throw new NotFoundError('User not found')
    }
    users.splice(userToDeleteIndex, 1)

    return res.status(200).json({ message: "User successfully deleted." })

})
//update

app.put('/users/:id', (req: Request, res: Response) => {
    //step 1: verify ID
    const { id } = req.params
    const userIndex = users.findIndex(user => user.id === id)
    const { email, age } = req.body
    if (userIndex === -1) {
        throw new NotFoundError('User not found')
    }

    //validate data sent in PUT request , email validation , includes @ 
    if (!email) {
        throw new BadRequestError('Email is required')
    }
    if (age && typeof age != "number") {
        throw new BadRequestError('Age must be a number')
    }

    const user = users[userIndex]
    user.email = email
    user.age = age

    return res.status(200).json({ message: "User updated.", user: user.getUser() });
})

function errorHandling(err: IError, req: Request, res: Response, next: NextFunction) {
    if (typeof err === 'object' && err.code) {
        return res.status(err.code).json({
            message: err.message,
            data: err.data
        })
    }

    return res.status(500).json({
        message: 'Something went wrong'
    })
}

app.use(errorHandling)

app.listen(PORT, () => {
    console.log(`App is running on port: ${PORT}`)
})