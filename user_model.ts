export interface IUser {
    email: string;
    hashedPassword: string;
    age?: number;
}

class User implements IUser {
    email: string;
    hashedPassword: string;
    age?: number
    id: string

    constructor(email: string, password: string, age?: number){
        this.email = email;
        this.hashedPassword = Buffer.from(password).toString('base64')
        this.age = age
        this.id = Date.now().toString()
    }

    getUser(){
        return {
            email: this.email,
            age: this.age,
            id: this.id
        }
    }
}

export default User